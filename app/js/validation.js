/*
jQuery.validator.setDefaults({
   debug: true,
   success: "valid"
});*/

$(function () {
   $('#profileForm').validate({
      rules: {
         prenom: {
            required: true,
            alphanumeric: true
         },
         nom: {
            required: true,
            alphanumeric: true
         },
         age: {
            required: true,
            plusPetite: true
         },
         statue: "required"
      },
      message: {
         prenom: {
            required: "Veuillez entrer votre prénom",
            alphanumeric: "Letters, numbers, and underscores only please"
         },
         nom: {
            required: "Veuillez entrer votre nom",
            alphanumeric: "Letters, numbers, and underscores only please"
         },
         age: {
            required: "entrez votre date de naissance",
            plusPetite: "La date de naissance ne peut pas etre inférieur à la date d'aujourd'hui"
         },
         statue: "veuilles selectionner un statue"

      },
      errorContainer: $('#errorContainer'),
      errorLabelContainer: $('#errorContainer ol'),
      wrapper: 'li'
   })
})
/*$(function () {
   $("#profileForm").onsubmit(function () {
      $('#formu').hide()
      $('#CÀ').fadeIn(250);
      form.submit();
      return false;
   });*/
}
//https://www.pierrefay.fr/blog/jquery-validate-formulaire-validation-tutoriel.html
$.validator.addMethod(
   "regex",
   function (value, element, regexp) {
      if (regexp.constructor != RegExp)
         regexp = new RegExp(regexp);
      else if (regexp.global)
         regexp.lastIndex = 0;
      return this.optional(element) || regexp.test(value);
   }, "erreur expression reguliere"
);

jQuery.validator.addMethod("alphanumeric", function (value, element) {
   return this.optional(element) || /^[\w.]+$/i.test(value);
}, "Letters, numbers, and underscores only please");

$.validator.addMethod("plusPetite", function (value, element) {
   let dateAujourdhui = new Date()
   return this.optional(element) || dateAujourdhui >= new Date(value)
}, "La date de naissance ne peut pas etre inférieur à la date d'aujourd'hui");