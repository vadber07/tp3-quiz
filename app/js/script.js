let profil = {}
let resultatQuiz
let score = 0;
let total = 2;

function calculAge() {
   let dateNaissance
   var dateAujourdhui = new Date();
   $('#age') = new Date(dateNaissance)
   let year = (dateAujourdhui - dateNaissance) / (1000 * 60 * 60 * 24 * 365)
   return parseInt(year)

}

/* validation du formulaire du profil */

$(function () {
   $("#formbutton").on('click', function () {
      formValidations()
   })


   $(function formValidations() {
      $('#profileForm').validate({
         rules: {
            nom: {
               required: true,
               alphanumeric: true
            },
            prenom: {
               required: true,
               alphanumeric: true
            },
            age: {
               required: true,
               plusPetite: true
            },
            statue: { required: true }
         },
         message: {
            prenom: {
               required: "Veuillez entrer votre prénom",
               alphanumeric: "Letters, numbers, and underscores only please"
            },
            nom: {
               required: "Veuillez entrer votre nom",
               alphanumeric: "Letters, numbers, and underscores only please"
            },
            age: {
               required: "entrez votre date de naissance",
               plusPetite: "La date de naissance ne peut pas etre inférieur à la date d'aujourd'hui"
            },
            statue: "veuilles selectionner un statue"
         },
         submitHandler: function (form) {
            profil.nom = $('#nom').val()
            profil.prenom = $('#prenom').val()
            profil.statue = $('#statue').val()
            profil.dateNaissance = $('#dateNaissance').val()
            $("#formu").remove()
            afficheProfile(profil)
            generationQuiz()
            creationQuestion(question)
         },

         showErrors: function (errorMap, errorList) {
            var sommaire = "Vous avez les erreurs: \n"
            const ul = $('<ul></ul>')
            $.each(errorList, function () { ul.append(`<li>${this.message}</li>`) });
            $('.listeDeserreurs').html(ul)
            $('.alert').show()
            this.defaultShowErrors();
         },
         invalidHandler: function (form, validator) {
            est_soumis = true;
         }

      })
   }),

      //https://www.pierrefay.fr/blog/jquery-validate-formulaire-validation-tutoriel.html
      $.validator.addMethod(
         "regex",
         function (value, element, regexp) {
            if (regexp.constructor != RegExp)
               regexp = new RegExp(regexp);
            else if (regexp.global)
               regexp.lastIndex = 0;
            return this.optional(element) || regexp.test(value);
         }, "erreur expression reguliere"
      );

   jQuery.validator.addMethod("alphanumeric", function (value, element) {
      return this.optional(element) || /^[\w.]+$/i.test(value);
   }, "Letters, numbers, and underscores only please");

   $.validator.addMethod("plusPetite", function (value, element) {
      let dateAujourdhui = new Date()
      return this.optional(element) || dateAujourdhui >= new Date(value)
   }, "La date de naissance ne peut pas etre inférieur à la date d'aujourd'hui");
})

/* generation de question du quiz */

function generationQuiz() {
   const questionsQuiz = `
   [
         {
            "question":"À quoi sert un aria-label?",
            "reponses":[
            "Ajouter du contenu textuel sur une balise
             pour aider les lecteurs d'écran",
            "À rien", 
            "Je ne sais pas"
            ]
         },
         {
            "question":"HTML vient de :",
            "reponses":[
               "Hyper Typo Meta Lol",
               "Hypertext markup language", 
               "Je ne sais pas"
            ]
         }
      ]
   `
   const donnees = JSON.parse(questionsQuiz)
   const accordeon = $('#accordeon')
   for (let i = 0; i < donnees.length; i++) {           //boucle sur les questions
      question = donnees[i]
      const div = $('<div class="quiz" id="C' + question.question[i] + '"></div>')
      $('<h2>' + question.question + '</h2>').appendTo(div)
      const form = $('<form></form>')
      const ul = $('<ul class="quiz" ></ul>')
      for (let j = 0; j < question.reponses.length; j++) {     // boucle sur les réponses pour une question donnée
         $('<li><input type="radio" id="R' + question.question[j] + '" name="reponceQ' + '" value="' + question.question[j] + '">' + '<label for="' + question.question[j] + '">' + question.reponses[j] + '</label></li></br>').appendTo(ul)
      }

      ul.appendTo(form),
         $('<input type="submit" id="submit" value="Suivant" />').appendTo(form),
         form.appendTo(div),
         div.appendTo(accordeon)
   } let as = question.question[j]

   $('#C' + as + '#submit').click(function () {
      $('.quiz').hide();
      analyse(CÀ);
      $('#C' + as).fadeIn(250);
      return false;
   })

   $('#CT #submit').click(function () {
      $('.quiz').hide();
      analyse(CT);
      $('#C' + as).fadeIn(250);
      return false;
   })
})

function init() {
   sessionPerso.setItem('bR1', 'À');
   sessionPerso.setItem('bR2', 'T');
}

const donnees = JSON.parse(donneesEntrantes)
$(document).ready(function () {
   creerTable(donnees)
});

function creerTable(donnees) {
   for (let i = 0; i < donnees.length; i++) {
      const questionCourante = donnees[i]
      const tr = $('<tr></tr>')
      tr.append('<td>' + (i + 1) + '</td>')  // le +1 est pour ne pas avoir de question #0 et les parenthèses sont obligatoires.
      tr.append('<td>' + questionCourante.question + '</td>')
      if (questionCourante.bonne === $('input[name=CÀ]:checked')) {
         tr.append('<td>' + 'crochet' + '</td>') // mettre des styles et un vrai crochet
      } else {
         tr.append('<td>X</td>') // mettre des styles
      }
      $('tbody').append(tr)
      const resultat = $('#resultat')
      const div = $('<div class="container"></div>')
      tr.appendTo(div)
      div.appendTo(resultat)
   }
}
$('.quiz #submit').click(function () {
   current = $(this).parents('form:first').data('question');
})
/* affichage du profil de participant */
function afficheProfile(profil) {
   $('#profil').append('<p>' + '<b> Nom : ' + profil.nom + '</p>')
   $('#profil').append('<p>' + '<b> Prénom : ' + profil.prenom + '</p>')
   $('#profil').append('<p>' + '<b> Age :  ' + calculAge(profil) + ' ' + 'ans' + '</p>')
   $('#profil').append('<p>' + '<b> Statue :  ' + profil.statue + '</p>')
   $('.ajoutLigne').append('<hr>')
}